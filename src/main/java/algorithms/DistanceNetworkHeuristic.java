package algorithms;

import main.PathInformation;

public class DistanceNetworkHeuristic extends SteinerAlgorithm {
    private final PathInformation[][] shortestDistanceMatrix;
    private final Graph D;

    public DistanceNetworkHeuristic(int k, double[][] matrix, PathInformation[][] minimalDistanceMatrix) {
        super(k, matrix);
        this.shortestDistanceMatrix = minimalDistanceMatrix;

        //Creates D(K)
        D = createD(minimalDistanceMatrix, k);
    }

    @Override
    public double resolve() {
        //Computes a MST for D
        Graph MST = new Kruskal(D).getMST();
        //Transform MST into a subgraph of G.
        Graph Td = new Graph();
        //replace every edge by the corresponding path.
        for (Edge edge : MST.getEdges())
            shortestDistanceMatrix[edge.from()][edge.to()].path().forEach(pathEdge -> Td.addEdge(pathEdge));

        //Compute a MST on Td
        Graph T = new Kruskal(Td).getMST();

        //Remove non-terminal leaves
        removeNonTerminalLeaves(T);

        return T.getTotalWeight();
    }
}

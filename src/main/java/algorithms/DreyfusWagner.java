package algorithms;

import main.PathInformation;
import org.apache.commons.math3.util.Combinations;

import java.util.*;
import java.util.stream.Collectors;

public class DreyfusWagner extends SteinerAlgorithm {
    private final PathInformation[][] minimalDistanceMatrix;
    private final Map<SubsetInfo, Double> S = new HashMap<>();
    private final Map<SubsetInfo, Double> Sv = new HashMap<>();

    public DreyfusWagner(int k, double[][] matrix, PathInformation[][] minimalDistanceMatrix) {
        super(k, matrix);
        this.minimalDistanceMatrix = minimalDistanceMatrix;
    }

    @Override
    public double resolve() {
        //Initialize the map.
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < n; j++) {
                Set<Integer> set = new HashSet<>();
                set.add(i);
                S.put(new SubsetInfo(set, j), minimalDistanceMatrix[i][j].totalDistance());
                set = new HashSet<>();
                set.add(j);
                S.put(new SubsetInfo(set, i), minimalDistanceMatrix[i][j].totalDistance());
            }
        }

        for (int i = 2; i < k; i++) {
            //We take subset of size i among k elements
            //We first calculate the Sv elements
            Iterator iterator = new Combinations(k, i).iterator();
            while (iterator.hasNext()) {
                //Creates the subset X according to the combination.
                Set<Integer> X = new HashSet<>();
                int[] array = (int[]) iterator.next();
                for (int val : array)
                    X.add(val);

                //We then take a random u and run the recursive algorithm
                for (int u = 0; u < n; u++) {
                    if (X.contains(u))
                        continue;
                    //We have a vertex u that is not in X
                    Sv.put(new SubsetInfo(X, u), computeMinSv(X, u));
                }
            }

            //We then calculate the S elements
            iterator = new Combinations(k, i).iterator();
            while (iterator.hasNext()) {
                Set<Integer> X = new HashSet<>();
                int[] array = (int[]) iterator.next();
                for (int val : array)
                    X.add(val);
                //We then take a random u and run the recursive algorithm
                for (int u = 0; u < n; u++) {
                    if (X.contains(u))
                        continue;
                    //We have a vertex u that is not in X
                    S.put(new SubsetInfo(X, u), computeMinS(X, u));
                }
            }
        }
        return S.get(new SubsetInfo(K.stream().filter(el -> el != 0).collect(Collectors.toSet()), 0));
    }

    /**
     * Goes through all the X' that could work and compute the min
     */
    private double computeMinSv(Set<Integer> X, int u) {
        Set<Integer> set = X;
        List<Set<Integer>> subsets = new ArrayList<>();
        subsets.add(new HashSet<>());
        computeSubsets(subsets, X);
        //We now have a list of all the subsets that could work
        //We loop through it and compute the min
        double min = Double.MAX_VALUE;
        for (Set<Integer> subset : subsets) {
            double weight = S.get(new SubsetInfo(subset, u)) + S.get(new SubsetInfo(complementary(X, subset), u));
            if (weight < min)
                min = weight;
        }
        return min;
    }


    /**
     * @return The value corresponding to S(X U u)
     */
    private double computeMinS(Set<Integer> X, int u) {
        //First we compute the min of S(X) +d(u,w) w with w in X.
        //We thus need to compute min d(u,w) with u in X
        double wInXMin = Double.MAX_VALUE;
        int wIndex = 0;
        for (int w : X) {
            double distance = minimalDistanceMatrix[u][w].totalDistance();
            if (distance < wInXMin) {
                wInXMin = distance;
                wIndex = w;
            }
        }
        Set<Integer> wIndexSet = new HashSet<>();
        wIndexSet.add(wIndex);
        wInXMin = wInXMin + S.get(new SubsetInfo(complementary(X, wIndexSet), wIndex));

        //We now compute the other minimum with w not in X.
        double wNotInXMin = Double.MAX_VALUE;
        for (int w : complementary(G, X)) {
            double distance = minimalDistanceMatrix[u][w].totalDistance();
            //(new SubsetInfo(X, w));
            //(Sv);
            double totalWeight = distance + Sv.get(new SubsetInfo(X, w));
            if (totalWeight < wNotInXMin) {
                wNotInXMin = totalWeight;
            }
        }
        return Math.min(wInXMin, wNotInXMin);
    }


    private void computeSubsets(List<Set<Integer>> subsets, Set<Integer> X) {
        for (int x : X) {
            List<Set<Integer>> newSubsets = new ArrayList<>();
            for (Set<Integer> set : subsets) {
                Set<Integer> set1 = new HashSet<>();
                set1.addAll(set);
                set1.add(x);
                newSubsets.add(set1);

            }
            subsets.addAll(newSubsets);
        }

        //At the end with remove X and the empty set
        subsets.remove(new HashSet<>());
        subsets.remove(X);
    }


    //It is not needed to redefine int hashCode(because used in an hashMap) as it is done automatically for records.
    private record SubsetInfo(Set<Integer> X, int v) {
    }


}

package algorithms;

import main.PathInformation;
import org.apache.commons.math3.util.Combinations;

import java.util.*;

public class EnumerationAlgorithm extends SteinerAlgorithm {

    private final Graph G; // Stores the graph G
    private final Graph D; // Stores the distance network of G
    private final PathInformation[][] minimalDistanceMatrix;


    public EnumerationAlgorithm(int k, double[][] matrix, PathInformation[][] minimalDistanceMatrix) {
        super(k, matrix); // le preprocessing est-il inclus ?
        this.minimalDistanceMatrix = minimalDistanceMatrix;
        this.D = createD(minimalDistanceMatrix, n);
        this.G = getG();
    }


    @Override
    public double resolve() {
        Graph best_tree = new Graph();
        double best_score = Double.MAX_VALUE;
        double score;
        // We iterate on all possible subsets S of branching points, with |S|<=k-2
        for (int s = 0; s <= k - 2; s++) {
            Iterator<int[]> iterator = new Combinations(n - k, s).iterator();
            while (iterator.hasNext()) {
                Set<Integer> S = new HashSet<Integer>(); // This is the set S\subset BP of branching (Steiner) points for our tree

                Graph sg = createD(minimalDistanceMatrix, n);
                int[] l = iterator.next();
                for (int i = 0; i < s; i++) {
                    S.add(k + l[i]);
                }

                // we add K to S to obtain K U S
                for (int i = 0; i < k; i++) {
                    S.add(i);
                }

                for (Edge e : D.getEdges()) {
                    if (!S.contains(e.from()) || !S.contains(e.to())) {
                        sg.removeEdge(e.from(), e.to());
                    }
                }

                Kruskal MST = new Kruskal(sg);
                Graph challenger = MST.getMST();
                score = challenger.getTotalWeight();
                if (score < best_score) {
                    best_score = score;
                    best_tree = challenger;
                }
            }
        }

        return best_score;
    }


}
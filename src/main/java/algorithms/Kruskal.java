package algorithms;

import java.util.Comparator;
import java.util.List;

public class Kruskal {


    // Edges array
    private final List<Edge> edges;

    private final Graph MST = new Graph();

    // Union-find data structure
    private int[] parent;

    // Kruskal's algorithm for finding the maximum spanning tree
    public Kruskal(Graph graph) {

        // Initialize the edges array
        edges = graph.getSortedEdgeList();
        int n = graph.getVertices().size();

        int max = 0;
        for (int vertex : graph.getVertices())
            if (vertex > max)
                max = vertex;
        // Initialize the union-find data structure.
        //Make it bigger than max value fo the vertex to not any ArrayOutOfBoundException
        parent = new int[max + 1];
        for (int i = 0; i < max+1; i++) {
            parent[i] = i;
        }

        // Keep track of the number of edges added to the MST
        int count = 0;

        // Iterate over the edges in non-decreasing order of weight
        for (Edge edge : edges) {
            // If the current edge connects two vertices in different sets
            if (find(edge.from()) != find(edge.to())) {
                // Add the edge to the MST
                MST.addEdge(edge);
                count++;

                // Union the sets that the edge connects
                union(edge.from(), edge.to());

                // If we have added n-1 edges, we have found the MST
                if (count == n - 1) {
                    break;
                }
            }
        }
    }

    // Finds the root of the set that x belongs to
    private int find(int x) {
        if (parent[x] != x) {
            parent[x] = find(parent[x]);
        }
        return parent[x];
    }

    // Unions the sets that x and y belong to
    private void union(int x, int y) {
        int rootX = find(x);
        int rootY = find(y);
        if (rootX != rootY) {
            parent[rootX] = rootY;
        }
    }

    public Graph getMST() {
        return MST;
    }


}
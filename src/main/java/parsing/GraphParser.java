package parsing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GraphParser {

    public ParsingResult parse(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        int k = (int) Double.parseDouble(reader.readLine());
        int n = (int) Double.parseDouble(reader.readLine());
        double[][] adjacency_matrix = new double[n][n];
        for (int i = 0; i < n; i++) {
            String[] values = reader.readLine().split("   ");
            //The first value is blank
            for (int j = 1; j < n+1; j++) {
                adjacency_matrix[i][j-1] = (Double.parseDouble(values[j]));

            }
        }
        return new ParsingResult(k, adjacency_matrix);

    }

}

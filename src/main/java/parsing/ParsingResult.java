package parsing;

public record ParsingResult(int k,double[][] adjacencyMatrix) {}
